import React, {
  useState,
  SetStateAction,
  Dispatch,
  useEffect,
} from 'react'
import axios from 'axios'

import { Profile, Transaction, CalculatedAccountIndex, User } from 'types'

type Props = {
  user: User
  children: any
}

type Context = {
  user: User
  profile?: Profile
  createTransaction: (tx: Transaction) => void
  deleteTransaction: (id: number) => void
  error?: string
  setError: Dispatch<SetStateAction<string | undefined>>
  selectProfile: (account: string) => void
  createProfile: (body: Profile) => void
  updateProfile: (id: string, body: Profile) => void
  balances?: CalculatedAccountIndex
  refresh: () => void
}

export const AppContext = React.createContext<Context | null>(null)

export const AppContextProvider = ({ children, user: userProp }: Props) => {
  // const navContext = useContext(NavContext)
  const [user, setUser] = useState<User>(userProp)
  const [error, setError] = useState<string>()
  const [txs, setTxs] = useState<Transaction[]>([])
  const [balances, setBalances] = useState<CalculatedAccountIndex>()
  const [selectedProfile, setSelectedProfile] = useState<string>()
  const [profile, setProfile] = useState<Profile>()

  useEffect(() => {
    // socket.on('refresh', refresh)
    // console.log('Socket connected!', socket.id)
  }, [])

  useEffect(() => {
    const get = async () => {
      if (!selectedProfile) return
      // set profile then transactionsIndex.

      const { data: profile } = await axios.get(
        `/api/accounts/${selectedProfile}`
      )
      setProfile(profile)

      const { data: transactions } = await axios.get(
        `/api/accounts/${selectedProfile}/transactions`
      )
      setTxs(transactions)
      // navContext?.setDestination('/')
    }
    get()
  }, [selectedProfile])

  useEffect(() => {
    // API get transactions
    const balances: CalculatedAccountIndex = {
      crap: [],
      groceries: [],
      takeout: [],
    }
    txs.forEach((tx: Transaction) => {
      balances[tx.accountName].push(tx)
    })
    setBalances(balances)
  }, [txs])

  useEffect(() => {
    if (profile) {
      console.log('profile:', profile)
    }
  }, [profile])

  const createProfile = async (body: Profile) => {
    const { data } = await axios.post(`/api/users/${user.id}/accounts`, body)
    await refreshUser() // to update accounts list.
    setSelectedProfile(data.id)
  }
  const updateProfile = async (id: string, body: Profile) => {
    await axios.put(`/api/accounts/${id}`, body)
    setSelectedProfile(id)
  }

  const selectProfile = (account: string) => {
    // if (account === selectedProfile) navContext?.setDestination('/')
    setSelectedProfile(account)
  }

  const createTransaction = async (tx: Transaction) => {
    if (!user) return
    await axios.post(`/api/accounts/${selectedProfile}/transactions`, tx)
    refresh()
  }

  const deleteTransaction = async (id: number) => {
    if (!user) return
    await axios.delete(`/api/accounts/${selectedProfile}/transactions/${id}`)
    refresh()
  }

  const refreshUser = async () => {
    const { data } = await axios.get(`/api/users/${user.id}`)
    setUser(data)
    return Promise.resolve()
  }
  const refresh = async () => {
    const { data: profile } = await axios.get(
      `/api/accounts/${selectedProfile}`
    )
    setProfile(profile)

    const { data: transactions } = await axios.get(
      `/api/accounts/${selectedProfile}/transactions`
    )
    setTxs(transactions)
  }

  return (
    <AppContext.Provider
      value={{
        balances,
        user,
        profile,
        updateProfile,
        createProfile,
        createTransaction,
        deleteTransaction,
        error,
        setError,
        selectProfile,
        refresh,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
