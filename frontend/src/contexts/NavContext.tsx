import React, { useState, useContext } from 'react'

type Context = {
  getDestination: () => string
  setDestination: (path: string) => void
}

export const NavContext = React.createContext<Context | null>(null)
export const NavContextProvider = (props: any) => {
  const [path, setPath] = useState<string | null>(null)
  const setDestination = (path: string) => {
    setTimeout(() => {
      setPath(path)
    }, 1000)
  }
  const getDestination = () => {
    if (!path) return
    const navigationPath = path
    setPath(null)
    return navigationPath
  }

  return (
    <NavContext.Provider
      value={{ getDestination, setDestination }}
      {...props}
    />
  )
}

export const useTransactionContext = () => useContext(NavContext)
