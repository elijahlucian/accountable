import React, { useState, useContext } from 'react'
import { AppContext } from 'contexts/AppContext'
import { AccountType } from 'types'
import { TransactionContext } from 'contexts/TransactionContext'

import 'scss/transaction-modal.scss'

type Props = {
  account: AccountType
}

export const TransactionModal = ({ account: propAccount }: Props) => {
  const [amount, setAmount] = useState('')
  const [account, setAccount] = useState(propAccount)
  const [details, setDetails] = useState('')
  const [error, setError] = useState<string | null>(null)
  const appContext = useContext(AppContext)
  const { toggleModal } = useContext(TransactionContext)

  // TODO: autocomplete 'what' field
  const handleAccountSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setAccount(e.target.value as AccountType)
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    if (!appContext) {
      setError('app context not loaded?')
      return
    }

    if (parseFloat(amount)) {
      appContext.createTransaction({
        accountName: account,
        details,
        amount: parseFloat(amount),
      })
      toggleModal()
    }
  }

  return (
    <div className="transaction-modal">
      <form onSubmit={handleSubmit} className="form">
        <div className="cancel-button" onClick={() => toggleModal()}>
          X
        </div>
        <h3>Expense for {account} account</h3>
        <label>
          Amount:
          <input
            autoFocus
            value={amount}
            onChange={e => setAmount(e.currentTarget.value)}
          ></input>
        </label>
        <label>
          Details:
          <input
            value={details}
            onChange={e => setDetails(e.currentTarget.value)}
          ></input>
        </label>
        <label>
          <select value={account} onChange={handleAccountSelect}>
            <option value={-1} disabled>
              Select Account
            </option>
            {[].map(v => (
              <option value={v} key={v}>
                {v}
              </option>
            ))}
          </select>
          <button type="button" onClick={() => toggleModal()}>
            cancel
          </button>
          <button type="submit">Submit</button>
        </label>
      </form>
      {error && <div className="error">{error}</div>}
    </div>
  )
}
