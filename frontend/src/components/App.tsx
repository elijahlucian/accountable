import React, { useContext } from 'react'
// import io from 'socket.io-client'
import { BrowserRouter, Switch, Link, Route, Redirect } from 'react-router-dom'

import { FundBar } from 'components/FundBar'
import { TransactionList } from 'components/TransactionList'
import { TransactionModal } from 'components/TransactionModal'
import { TransactionContext } from 'contexts/TransactionContext'
import { AccountForm } from 'components/AccountForm'
import { AccountSelect } from 'components/AccountSelect'
import { UserForm } from 'components/UserForm'

import { AppContext } from 'contexts/AppContext'
import { NavContext } from 'contexts/NavContext'
import { StatusContext } from 'contexts/StatusContext'

import 'scss/app.scss'
import 'scss/status.scss'

export const App = () => {
  const appContext = useContext(AppContext)
  const { account, showingModal } = useContext(TransactionContext)
  const status = useContext(StatusContext)?.getStatus()
  const destination = useContext(NavContext)?.getDestination()

  if (!appContext?.user) return <div>something went wrong!</div>

  return (
    <div className="app" id="appElement">
      {status}
      <BrowserRouter>
        {!appContext?.profile && <Redirect to="/select" />}
        {destination && <Redirect to={destination} />}

        <nav>
          <Link to="/">Home</Link>
          <Link to="/select">Select Budget</Link>
          {appContext?.user?.accounts?.length ? (
            <Link to="/account">Budget Details</Link>
          ) : (
            undefined
          )}
          <Link to="/details">Transactions</Link>
          <Link to="/user">Profile</Link>
        </nav>

        <Switch>
          <Route path="/user">
            <UserForm user={appContext.user} />
          </Route>
          <Route path="/details">
            <TransactionList />
          </Route>
          <Route path="/select">
            <AccountSelect
              user={appContext.user}
              selectProfile={appContext.selectProfile}
            />
          </Route>
          <Route path="/account/new">
            <AccountForm user={appContext.user} />
          </Route>
          <Route path="/account">
            <AccountForm profile={appContext.profile} user={appContext.user} />
          </Route>
          <Route path="/">
            <>
              {!appContext?.user?.accounts?.length && (
                <Redirect to="/account/new" />
              )}
              <h1>Remaining Balances</h1>
              <div className="funds">
                <FundBar account="groceries" label="Groceries" col={1} />
                <FundBar account="takeout" label="Takeout" col={2} />
                <FundBar account="crap" label="Crap" col={3} />
              </div>
              {showingModal && <TransactionModal account={account} />}
            </>
          </Route>
        </Switch>

        <footer>
          <p>User: {appContext.user.name}</p>
          <p>|</p>
          <p>Budget: {appContext?.profile?.name}</p>
          <p>|</p>
        </footer>
      </BrowserRouter>
    </div>
  )
}
