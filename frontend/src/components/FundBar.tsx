import React, { useContext } from 'react'
import { TransactionContext } from 'contexts/TransactionContext'

import { AppContext } from 'contexts/AppContext'
import { AccountType } from 'types'

import 'scss/fund-bar.scss'

type Props = {
  label: string
  col: number
  account: AccountType
}

export const FundBar = ({ label, col, account }: Props) => {
  const appContext = useContext(AppContext)
  const { toggleModal } = useContext(TransactionContext)

  if (!appContext || !appContext?.profile || !appContext?.balances)
    return <div>Loading</div>

  // TODO: YUCK - server response / context
  const amount = 0
  
  // appContext.balances[account].length
  //   ? appContext.balances[account].reduce(
  //       ({ amount: prevAmount, ...prev }, { amount, ...curr }) => {
  //         return { ...curr, amount: prevAmount + amount }
  //       }
  //     ).amount
  //   : 0

  const max = appContext?.profile[account]
  const current = max - amount
  const percent = Math.max(current / max, 0)
  const hue = percent * 120

  return (
    <>
      <div
        className={`fundbar back col${col}`}
        onClick={() => toggleModal(account)}
      ></div>
      <div
        className={`fundbar front col${col}`}
        style={{
          background: `hsl(${hue}, 100%, 50%)`,
          height: `${percent * 40 + 10}vmin`,
        }}
      >
        <h3>{label}</h3>
      </div>
      <div
        className={`fundbar totals col${col}`}
        style={{ color: `hsl(0, 0%, ${Math.abs(1 - percent) * 100}%)` }}
      >
        ${Math.floor(current)} / ${max}
      </div>
    </>
  )
}
