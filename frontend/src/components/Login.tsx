import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import { Link } from 'react-router-dom'

type Props = {
  handleLogin: (v: string) => void
}

export const Login = ({ handleLogin }: Props) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [valid, setValid] = useState(false)
  const [error, setError] = useState('')

  useEffect(() => {
    if (window.localStorage.userId) handleLogin(window.localStorage.userId)
  }, [handleLogin])

  useEffect(() => {
    email && password ? setValid(true) : setValid(false)
  }, [email, password])

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    if (process.env.NODE_ENV === 'development') {
      handleLogin(email)
      return
    }
    if (!email || !password) {
      setError('please fill out both fields')
      return
    }
    try {
      const { data } = await Axios.post('/api/login', { email, password })
      handleLogin(data.id)
      window.localStorage.userId = data.id
    } catch (err) {
      setError(err.message)
    }
  }

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <label>
          Type Yo Email:
          <input
            autoFocus
            onChange={e => setEmail(e.target.value)}
            value={email}
          ></input>
        </label>
        <label>
          Type Yo Password:
          <input
            type="password"
            onChange={e => setPassword(e.target.value)}
            value={password}
          ></input>
        </label>
        <label>
          <button disabled={!valid} type="submit">
            Submit
          </button>
        </label>
        {error && <p>{error}</p>}
        <Link to="/sign-up">No Account? Sign Up!</Link>
      </form>
    </div>
  )
}
