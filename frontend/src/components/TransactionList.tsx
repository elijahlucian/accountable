import React from 'react'

// import { AppContext } from 'contexts/AppContext'
import 'scss/transaction-list.scss'

export const TransactionList = () => {
  // const appContext = useContext(AppContext)

  return (
    <>
      <h1>Transactions</h1>
      <div className="transactions">
        {[].map((account, i) => {
          return (
            <div className="column" key={`${account}-txs-${i}`}>
              <h3>{account}</h3>
              {/* {appContext?.balances &&
                appContext.balances[account]?.map((tx: any) => (
                  <p className="transaction-item">
                    <p className="amount">${tx.amount}</p>
                    <p className="details">{tx.details}</p>
                    <button
                      onClick={() =>
                        tx.id && appContext.deleteTransaction(tx.id)
                      }
                    >
                      X
                    </button>
                  </p>
                ))} */}
            </div>
          )
        })}
      </div>
    </>
  )
}
