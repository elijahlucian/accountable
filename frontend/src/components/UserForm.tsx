import React, { useState, useEffect } from 'react'
import Axios from 'axios'

import { User } from 'types'
import 'scss/form.scss'

type Props = {
  user?: User
  handleLogin?: (id: string) => void
}

export const UserForm = ({ user, handleLogin }: Props) => {
  const [name, setName] = useState(user?.name)
  const [email, setEmail] = useState(user?.email)
  const [password, setPassword] = useState(user?.password)
  const [passwordConfirm, setPasswordConfirm] = useState('')
  const [valid, setValid] = useState(false)

  useEffect(() => {
    passwordConfirm.length > 4 && password === passwordConfirm && name && email
      ? setValid(true)
      : setValid(false)
  }, [password, passwordConfirm, name, email])

  // @ts-ignore
  const handleSubmit = async e => {
    e.preventDefault()

    const body = {
      name,
      email,
      password,
    }

    try {
      const { data } = user?.id
        ? await Axios.put(`/api/users/${user.id}`, body)
        : await Axios.post('/api/users', body)
      if (handleLogin) handleLogin(data.id)
    } catch (err) {
      // TODO: status bar
    }
  }

  return (
    <>
      <h1>{user?.id ? 'Edit' : 'Create'} Profile</h1>
      <form className="dank-form" onSubmit={handleSubmit}>
        <label>
          Name: <input value={name} onChange={e => setName(e.target.value)} />
        </label>
        <label>
          Email:
          <input
            value={email}
            type="email"
            onChange={e => setEmail(e.target.value)}
          />
        </label>
        <label>
          Password:
          <input
            value={password}
            type="password"
            onChange={e => setPassword(e.target.value)}
          />
        </label>
        <label>
          Confirm Password:
          <input
            type="password"
            onChange={e => setPasswordConfirm(e.target.value)}
          />
        </label>
        <button type="submit" disabled={!valid}>
          Save
        </button>
      </form>
    </>
  )
}
