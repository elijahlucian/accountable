import { Profile, Transaction } from "types"
import { DataBuddy } from '@dank-inc/data-buddy'

type ApiParams = {
  mock?: true
  accounts: 
}

export interface Api {
  mock: true
  accounts: DataBuddy
} | {
  mock: undefined
}

export class Api {
  constructor({mock, accounts, profiles, transactions}: ApiParams) {
    this.mock = mock
    this.accounts = accounts
    this.profiles = profiles
    this.transactions = transactions
  }

  getUser = (id: string) => {}

  getAccounts = (userId: string) => {}
  
  getProfiles = (userId: string): Profile[] => {

    return []
  }

  getTransactions = (accountId: string): Transaction[] => {

    return []
  }

  addTransaction = (accountId: string) => {}

}