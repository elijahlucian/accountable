export type AccountType = 'groceries' | 'takeout' | 'crap'

export type CalculatedAccountIndex = Record<AccountType, Transaction[]>

export type Transaction = {
  id?: number
  amount: number
  details: string
  accountName: AccountType
  accountId?: string
}

export type Profile = {
  id: string
  name: string
  income: number
  expenses: number
  details?: string
  // transactions: Transaction[]
}

export type User = {
  id: string
  name: string
  email: string
  password: string
  accounts: Profile[]
}
