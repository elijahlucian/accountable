import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import axios from 'axios'
import * as serviceWorker from './serviceWorker'
import './index.css'

import { App } from 'components/App'
import { Login } from 'components/Login'
import { UserForm } from 'components/UserForm'

import { TransactionContextProvider } from 'contexts/TransactionContext'
import { AppContextProvider } from 'contexts/AppContext'
import { NavContextProvider } from 'contexts/NavContext'
import { StatusContextProvider } from 'contexts/StatusContext'

import { AccountType, User } from 'types'

export const accountTypes: AccountType[] = ['groceries', 'takeout', 'crap']

const AppWithContext = () => {
  const [user, setUser] = useState<User>()

  const handleLogin = async (id: string) => {
    const { data } = await axios.get(`/api/users/${id}/`)
    setUser(data)
  }

  return !user ? (
    <BrowserRouter>
      <Switch>
        <Route path="/sign-up">
          <UserForm handleLogin={handleLogin} />
        </Route>
        <Route path="/">
          <Login handleLogin={handleLogin} />
        </Route>
      </Switch>
    </BrowserRouter>
  ) : (
    <StatusContextProvider>
      <NavContextProvider>
        <AppContextProvider user={user}>
          <TransactionContextProvider>
            <App />
          </TransactionContextProvider>
        </AppContextProvider>
      </NavContextProvider>
    </StatusContextProvider>
  )
}

ReactDOM.render(<AppWithContext />, document.getElementById('root'))
serviceWorker.unregister()
