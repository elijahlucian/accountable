# ACCOUNTABLE - A ridiculously simple way to save money

# What is it?
Accountable is basically a smart cash budget, that tracks your spending into a few simple folders. 

forget micro-managing all your expenses, just simply put in your total income and expenses into accountable and start saving money instantly!

## DEV SETUP

- dcrr backend npm run db:init

## DEPLOY

## TODO:

MVP:

- responsive styling
  - fund bars are too wide
  - transaction list , full width

Milestone 1:

- net balance (in case one goes over.)
  - in status bar?
- "look how much you saved!"

pro subscription:

- months back and forth (show current month in status bar)
- make a trend view.

- test after modifying profile details, update profile
- test after modifying account details, update account

- websocket listner for transactions.

- add logout button
- progressive web app

Milestone 2:

- entrepeneur mode
- submitting income adds to budget bars
- kind of a gameified approach to budgeting

Maybe:

- dockerize
- docker compose

## DONE:

- save user id to localstorage for now for re-logging in
- new user workflow

  - login
    - sign up!

- add password to login page.
- add login route
- on login put the username in local storage for now
- deploy on heroku
- proper menubar
- new page - edit account
- add redirect after selecting, editing, and creating
- account tweaks
  - add monthly income field
  - add monthly expenses field
