'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
        id: 'elijah',
        name: 'elijah',
        email: 'elijahlucian@gmail.com',
        password: 'toffee15',
        createdAt: new Date('2020 02 15').toISOString(),
        updatedAt: new Date('2020 02 15').toISOString(),
      },
      {
        id: 'kaela',
        name: 'kaela',
        email: 'kaelacaron@gmail.com',
        password: 'wicked',
        createdAt: new Date('2020 02 15').toISOString(),
        updatedAt: new Date('2020 02 15').toISOString(),
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {})
  },
}
