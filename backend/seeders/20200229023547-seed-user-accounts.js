'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
    return queryInterface.bulkInsert(
      'user_accounts',
      [
        {
          userId: 'elijah',
          accountId: 'kande',
          createdAt: new Date('2020 02 15').toISOString(),
          updatedAt: new Date('2020 02 15').toISOString(),
        },
        {
          userId: 'kaela',
          accountId: 'kande',
          createdAt: new Date('2020 02 15').toISOString(),
          updatedAt: new Date('2020 02 15').toISOString(),
        },
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_accounts', null, {})
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  },
}
