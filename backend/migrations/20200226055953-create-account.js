'use strict'
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('accounts', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      details: {
        type: Sequelize.STRING,
      },
      groceries: {
        type: Sequelize.FLOAT,
      },
      takeout: {
        type: Sequelize.FLOAT,
      },
      crap: {
        type: Sequelize.FLOAT,
      },
      income: {
        type: Sequelize.FLOAT,
      },
      expenses: {
        type: Sequelize.FLOAT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('accounts')
  },
}
