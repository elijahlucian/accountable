'use strict'
module.exports = (sequelize, DataTypes) => {
  const UserAccount = sequelize.define(
    'user_account',
    {
      accountId: DataTypes.STRING,
      userId: DataTypes.STRING,
    },
    {}
  )

  UserAccount.associate = ({ user, account }) => {
    UserAccount.belongsTo(user, {
      as: 'user',
      foreignKey: 'userId',
    })
    UserAccount.belongsTo(account, {
      as: 'account',
      foreignKey: 'accountId',
    })
  }
  return UserAccount
}
