'use strict'

module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define(
    'account',
    {
      name: DataTypes.STRING,
      details: DataTypes.STRING,
      crap: DataTypes.FLOAT,
      groceries: DataTypes.FLOAT,
      takeout: DataTypes.FLOAT,
      income: DataTypes.FLOAT, // HOUSEHOLD INCOME
      expenses: DataTypes.FLOAT, // MONTHLY EXPENSES
    },
    {}
  )
  Account.associate = ({ transaction, user }) => {
    Account.hasMany(transaction, {
      foreignKey: 'accountId',
      as: 'transactions',
    })
    Account.belongsToMany(user, {
      as: 'users',
      through: 'user_accounts',
      foreignKey: 'accountId',
    })
    // associations can be defined here
  }
  return Account
}
