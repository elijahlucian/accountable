"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express = require('express');
var dotenv = require('dotenv').config();
var socket = require('socket.io');
var path = require("path");
var moment = require('moment');
var Sequelize = require('sequelize');
var App = /** @class */ (function () {
    function App() {
        this.server = express();
        this.server.use(express.static(__dirname + '/../public'));
        this.server.use(express.urlencoded({ extended: true }));
        this.server.use(express.json());
        this.db = require('../models');
        this.io = socket();
        console.log(this.db ? '>> db connected!' : '>> db did not connect!!!');
        this.initSockets();
        this.init();
        this.server.get('*', function (req, res) {
            res.sendFile(path.join(__dirname, '/../public/index.html'));
        });
    }
    App.prototype.initSockets = function () {
        console.log('initializing sockets');
        this.io.on('connection', function (socket) {
            console.log('user connected', socket.id);
        });
    };
    App.prototype.init = function () {
        var _this = this;
        this.server.post('/api/login', function (_a, res) {
            var _b = _a.body, email = _b.email, password = _b.password;
            return __awaiter(_this, void 0, void 0, function () {
                var record;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!email || !password) {
                                res.status(422);
                                res.send({ message: 'you must supply a email and password!' });
                                return [2 /*return*/];
                            }
                            return [4 /*yield*/, this.db.user.findOne({ where: { email: email } })];
                        case 1:
                            record = _c.sent();
                            if ((record === null || record === void 0 ? void 0 : record.password) === password) {
                                res.send(record);
                            }
                            else {
                                res.status(403);
                                res.send({ message: "Ain't the right password, dog!" });
                            }
                            return [2 /*return*/];
                    }
                });
            });
        });
        // USERS
        this.server.get('/api/users/:id?', function (_a, res) {
            var id = _a.params.id;
            return __awaiter(_this, void 0, void 0, function () {
                var q, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!id) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.db.user.findOne({
                                    where: { id: id },
                                    include: ['accounts']
                                })];
                        case 1:
                            _b = _c.sent();
                            return [3 /*break*/, 4];
                        case 2: return [4 /*yield*/, this.db.user.findAll({ include: ['accounts'] })];
                        case 3:
                            _b = _c.sent();
                            _c.label = 4;
                        case 4:
                            q = _b;
                            this.handleQuery(q, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.post('/api/users', function (_a, res) {
            var body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                var id, q;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            console.log('CREATE USER =>', body.name);
                            id = body.name.replace(/\W/g, '') + Math.floor(Math.random() * 100000);
                            return [4 /*yield*/, this.db.user.create(__assign(__assign({}, body), { id: id }))];
                        case 1:
                            q = _b.sent();
                            this.handleQuery(q, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.put('/api/users/:id', function (_a, res) {
            var id = _a.params.id, body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                var record, q;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, this.db.user.findOne({ where: { id: id } })];
                        case 1:
                            record = _b.sent();
                            return [4 /*yield*/, record.update(__assign({}, body))];
                        case 2:
                            q = _b.sent();
                            this.handleQuery(q, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        // ACCOUNTS
        this.server.post('/api/users/:id/accounts', function (_a, res) {
            var id = _a.params.id, body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                var user, accountId, account;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            console.log('CREATE ACCOUNT =>', body.name);
                            return [4 /*yield*/, this.db.user.findOne({ where: { id: id } })];
                        case 1:
                            user = _b.sent();
                            accountId = id + body.name.replace(/\W/g, '');
                            return [4 /*yield*/, user.createAccount(__assign(__assign({}, body), { id: accountId }))];
                        case 2:
                            account = _b.sent();
                            this.handleQuery(account, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.get('/api/accounts/:id?', function (_a, res) {
            var id = _a.params.id;
            return __awaiter(_this, void 0, void 0, function () {
                var q, _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!id) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.db.account.findOne({ where: { id: id }, include: ['users'] })];
                        case 1:
                            _b = _c.sent();
                            return [3 /*break*/, 4];
                        case 2: return [4 /*yield*/, this.db.account.findAll({ include: ['users'] })];
                        case 3:
                            _b = _c.sent();
                            _c.label = 4;
                        case 4:
                            q = _b;
                            this.handleQuery(q, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.put('/api/accounts/:id', function (_a, res) {
            var id = _a.params.id, body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                var q;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, this.db.account.update(__assign({}, body), { where: { id: id } })];
                        case 1:
                            q = _b.sent();
                            this.handleQuery(q, res);
                            this.io.emit(id, 'refresh'); // TODO: try dis
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.post('/api/accounts', function (_a, res) {
            var body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                var q;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, this.db.account.create(__assign({}, body))];
                        case 1:
                            q = _b.sent();
                            this.handleQuery(q, res);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.get('/api/accounts/:id/transactions/:year?/:month?', function (_a, res) {
            var id = _a.params.id;
            return __awaiter(_this, void 0, void 0, function () {
                var startOfMonth, endOfMonth, q;
                var _b;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            startOfMonth = moment()
                                .startOf('month')
                                .toDate();
                            endOfMonth = moment()
                                .endOf('month')
                                .toDate();
                            console.log('Getting from', startOfMonth, 'to', endOfMonth);
                            return [4 /*yield*/, this.db.transaction.findAll({
                                    where: {
                                        accountId: id,
                                        createdAt: (_b = {}, _b[Sequelize.Op.between] = [startOfMonth, endOfMonth], _b)
                                    }
                                })
                                // TODO: where year and month are in this range.
                            ];
                        case 1:
                            q = _c.sent();
                            // TODO: where year and month are in this range.
                            res.send(q);
                            return [2 /*return*/];
                    }
                });
            });
        });
        this.server.post('/api/accounts/:id/transactions', function (_a, res) {
            var id = _a.params.id, body = _a.body;
            return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_b) {
                    console.log('new tx =>', id, body);
                    this.db.transaction.create(__assign(__assign({}, body), { accountId: id }));
                    res.send({ body: body, id: id });
                    this.io.emit(id, 'refresh');
                    return [2 /*return*/];
                });
            });
        });
    };
    App.prototype.handleQuery = function (record, res) {
        if (record) {
            res.send(record);
        }
        else
            this.notFound(res);
    };
    App.prototype.handleQueryAll = function (records, res) {
        if (records.length) {
            res.send(records);
        }
        else
            this.notFound(res);
    };
    App.prototype.notFound = function (res) {
        res.send({ message: 'not found' });
    };
    App.prototype.start = function () {
        var port = process.env.PORT;
        this.server.listen(port);
        console.log("listening on " + port + "!");
    };
    return App;
}());
var app = new App();
app.start();
