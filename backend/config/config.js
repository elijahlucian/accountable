const dotenv = require('dotenv').config()

module.exports = {
  development: {
    use_env_variable: 'DATABASE_URL',
    // url:
    //   'postgres://three_accounts:three_accounts@localhost:5432/three_accounts',
    // database: process.env.DB_NAME,
    // username: process.env.DB_USERNAME,
    // password: process.env.DB_PASSWORD,
    dialect: 'postgres',
  },
  test: {
    dialect: 'sqlite',
    storage: './test.sqlite3',
  },
  production: {
    use_env_variable: 'DATABASE_URL',
    // url: process.env.DATABASE_URL,
    ssl: true,
    // database: process.env.DB_NAME,
    // username: process.env.DB_USERNAME,
    // password: process.env.DB_PASSWORD,
    // host: process.env.DB_HOST,
    // port: process.env.DB_PORT,
    dialect: 'postgres',
    // logging: true,
  },
}
